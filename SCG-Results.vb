Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile

Public Class SCG_Results
    ' -----------------------------------------------------------------------------
    ' Class: SCG_Results
    '
    ' Description:
    '               This class does everything that has to do with results from skating events.
    '               
    ' Parameters:
    '               
    '
    ' Returns:
    '               
    '
    ' Notes:
    '               
    ' -----------------------------------------------------------------------------

    Protected Class athlete
        'General
        Public rank As String = Nothing
        Public bib As String = Nothing
        Public name As String = Nothing
        Public nat_code As String = Nothing
        Public nation As String = Nothing
        'Speedskating
        Public time As String = Nothing
        Public diff As String = Nothing

        'Construktor 
        'Speedskating
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal diff_in As String)
            ' -----------------------------------------------------------------------------
            ' Function: NEW (the constructor of this class athlete)
            '
            ' Description:
            '               This function sets up the speedskating athlete-class. 
            '               
            ' Parameters:
            '               rank_in: Which position the athlete got
            '               bib_in: Which startnumber the athlete has
            '               name_in: The name of the athlete
            '               natcode_in: Nation code, which nation the athlete is from
            '               time_in: Which time the athlete got
            '               diff_in: How far behind he / she is the winner.
            '
            ' Returns:
            '               Nothing
            '
            ' Notes :
            '               
            ' -----------------------------------------------------------------------------

            Dim logFolder As String = My.Settings.LogFolder ' MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
            Dim Debug As Boolean = My.Settings.Debug ' MainConvertModule.ProjectFolder(AppSettings("Debug"))
            If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.sub New")
            rank = rank_in
            If Debug = True Then WriteLog(logFolder, "Vi har: Rank" & rank)
            bib = bib_in
            If Debug = True Then WriteLog(logFolder, "Vi har bib: " & bib)
            name = name_in
            If Debug = True Then WriteLog(logFolder, "Vi har name: " & name)
            nat_code = natcode_in
            If Debug = True Then WriteLog(logFolder, "Vi har nat_code: " & nat_code)
            diff = diff_in
            If Debug = True Then WriteLog(logFolder, "Vi har diff: " & diff)
            If Debug = True Then WriteLog(logFolder, "Vi har: " & rank & " " & bib & " " & name & " " & nat_code & " " & diff)

            nation = MainConvertModule.nation_map(nat_code)
            If nation = "" Then nation = natcode_in
            If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation)
            time = time_in

            Dim lastNameFirst As Boolean
            lastNameFirst = False
            If nation Like "Kina" Or nation Like "S�r-Korea" Or nation Like "Nord-Korea" Then
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til true")
                lastNameFirst = True
            Else
                If Debug = True Then WriteLog(logFolder, "Vi har nation: " & nation & " og setter lastnamefirst til false")
                lastNameFirst = False
            End If
            If Debug = True Then WriteLog(logFolder, "Vi snur navn")
            name = ConvertTools.RotateAndFixName(name, , lastNameFirst)
            If Debug = True Then WriteLog(logFolder, "Vi har name: " & name)


            If Debug = True Then WriteLog(logFolder, "Character map substitutions")
            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Debug = True Then WriteLog(logFolder, "subst")
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
                If Debug = True Then WriteLog(logFolder, "Ferdig med for loop")
            End If

        End Sub

    End Class

    Protected athlete_list As ArrayList
    Protected Shared character_map As New Hashtable

    Public Sub New()
        ' -----------------------------------------------------------------------------
        ' Function: New (constructor of class SCG_Results)
        '
        ' Description:
        '               Object constructor creates object and loads mapping lists
        '               
        ' Parameters:
        '               
        '
        ' Returns:
        '               
        '
        ' Notes :
        '               
        ' -----------------------------------------------------------------------------

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = My.Settings.CharacterMapFile ' MainConvertModule.ProjectFolder(AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(My.Settings.LogFolder), "SCG - Character converison map loaded (" & charmapfile & "): " & CStr(character_map.Count) & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(My.Settings.LogFolder), "Error loading charachter conversion map (" & charmapfile & ") for IBU", ex)
        End Try

    End Sub

    'Takes a speedskating HTML list and fills the athlete list
    Protected Function ParseDataSCG(ByVal filename As String) As Integer
        ' -----------------------------------------------------------------------------
        ' Function: ParseDataSCG
        '
        ' Description:
        '               This function parses the data we get from the HTML-file. It then returns
        '               a list of athletes which will then be saved in a file. 
        '               
        ' Parameters:
        '               filename: String containing the filename of the file we are to parse
        '               
        '
        ' Returns:
        '               This function returns a list of athletes that we put in a resultslist in SCGGeneral
        '
        ' Notes :
        '               
        ' -----------------------------------------------------------------------------

        Dim logFolder As String = My.Settings.LogFolder ' MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = My.Settings.Debug ' MainConvertModule.ProjectFolder(AppSettings("Debug"))
        If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.ParseDataSCG")

        Dim input As StreamReader = New StreamReader(filename, Encoding.GetEncoding("iso-8859-1"))
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        If Debug = True Then WriteLog(logFolder, "Remove tags")
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")

        'Split off header
        If Debug = True Then WriteLog(logFolder, "Split off header")
        Dim topsplit As String = "Rankings"
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        If Debug = True Then WriteLog(logFolder, "Find results with RegExp")
        Dim res As Regex = New Regex("(\d+)\s+(\d+)\s+([\w\- ]+?)\s+(\w{3})\s+([\d:\.]+)\s+([\d:\.]+)?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)
        Dim first As TimeSpan
        Dim m As Match = res.Match(content)
        While m.Success

            Dim rank As String = m.Groups(1).Value
            Dim bib As String = m.Groups(2).Value
            Dim name As String = m.Groups(3).Value
            Dim nation As String = m.Groups(4).Value

            If Debug = True Then WriteLog(logFolder, "Data vi har: " & rank & " " & bib & " " & name & " " & nation)

            'Fetch the time data
            Dim time, diff As TimeSpan
            If m.Groups(6).Value = "" Then
                first = GetTimeSpan(m.Groups(5).Value, 10)
                time = first
            Else
                diff = GetTimeSpan(m.Groups(6).Value, 10)
                time = first.Add(diff)
            End If

            'Format times by calling the formattimeskate function. 
            Dim strTime, strDiff As String
            strTime = ConvertTools.FormatTimeSkate(time, 10)
            strDiff = ConvertTools.FormatTimeSkate(diff, 10)

            If Debug = True Then WriteLog(logFolder, "Mer data: " & strTime & " " & strDiff)

            If Debug = True Then WriteLog(logFolder, "Lager ny athlete")
            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff)
            If Debug = True Then WriteLog(logFolder, "Ferdig med � lage ny athlete")

            If Debug = True Then WriteLog(logFolder, "Legger til i athlete_list")

            athlete_list.Add(athlete)
            If Debug = True Then WriteLog(logFolder, "Lagt til i athlete_list")
            m = m.NextMatch()
        End While
        If Debug = True Then WriteLog(logFolder, "Antall ut�vere: " & athlete_list.Count)
        Return athlete_list.Count
    End Function

    Public Function SCG_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As String
        ' -----------------------------------------------------------------------------
        ' Function: SCG_General
        '
        ' Description:
        '               Returns a formated resultlist, for individual starts
        '               
        ' Parameters:
        '               filename:   The filename of the file where all the data is stored (a HTML-file)
        '               format:     Which formattype is it that we are working with.
        '               body:       String used to return what we get in this function. 
        '
        ' Returns:
        '               Returns a resultlist based on the data in the HTML-file. 
        '
        ' Notes :
        '               
        ' -----------------------------------------------------------------------------

        Dim logFolder As String = My.Settings.LogFolder ' MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = My.Settings.Debug ' MainConvertModule.ProjectFolder(AppSettings("Debug"))
        If Debug = True Then WriteLog(logFolder, "Er i SCGconvert.SCG_General")

        'Fill the athlete list
        Dim c As Integer = ParseDataSCG(filename)
        Dim ret As String = Nothing

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraphs
            If i = 1 Then
                ' ret &= "<p class=""Brdtekst"">"
                ret &= "<p class=""txt"">"
            ElseIf i Mod 10 = 1 Then
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.IndividualStart
                    ' Puts togehter a string that will look like this: 1) Jon Jonsen, USA 0.22,43,
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    ElseIf i = 2 Then
                        ' I really don't see a point for this I=2 check, but it must be here for a reason.
                        ' So when I get back to work, I will check by removing this line if it has any effect.
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & ", "
                    End If
                    '    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    'Case Else
                    '    If i = 1 Then
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    '    ElseIf i = 2 Then
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " min bak (" & at.time & "), "
                    '    Else
                    '        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.time & "), "
                    '    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function


End Class
