Imports System.IO
Imports System.xml
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile

Public Class IBU_Results
    ' -----------------------------------------------------------------------------
    ' Class: IBU_Results
    '
    ' Description:
    '               This class contains all functions to parse results form Biathlon events organised by IBU.
    '
    ' Parameters:
    '               
    '
    ' Returns:
    '               This class returns a list of results based on the data in the HTML-file provided.
    '               
    '
    ' Notes :
    '               We must add a function to parse biathlon relay events.
    ' -----------------------------------------------------------------------------

    Protected Class athlete

        'General
        Public rank As String
        Public bib As String
        Public name As String
        Public nat_code As String
        Public nation As String

        'Biathlon specifics
        Public start As String
        Public time As String
        Public diff As String
        Public shoot_1 As String
        Public shoot_2 As String
        Public shoot_3 As String
        Public shoot_4 As String
        Public shoot_total As String

        Public points_total As String

        'Constructors
        'Biathlon
        Public Sub New(ByVal rank_in As String, ByVal bib_in As String, ByVal name_in As String, ByVal natcode_in As String, ByVal time_in As String, ByVal diff_in As String, _
                       ByVal shoot_1_in As String, ByVal shoot_2_in As String, ByVal shoot_3_in As String, ByVal shoot_4_in As String, ByVal shoot_total_in As String)
            ' -----------------------------------------------------------------------------
            ' Function:  NEW (The constructor of the IBU-athlete) (biathlon athelete that is)
            '
            ' Description:
            '               This is the constructor-function for the biathlon athlete 
            '               
            ' Parameters:
            '               rank_in: Which rank the athlete got (1st for instance)
            '               bib_in: Which startnumber the athlete had
            '               name_in: Name of the athlete
            '               natcode_in: From which nation the athlete is from (normally like this: NOR)
            '               time_in: The time the athlete got
            '               diff_in: How far behind the athlete was the winner.
            '               shoot_1_in: number of misses in leg 1 (eg. 2)
            '               shoot_2_in: number of misses in leg 2
            '               shoot_3_in: number of misses in leg 3
            '               shoot_4_in: number of misses in leg 4
            '               shoot_total_in: number of misses total 
            '               
            '
            ' Returns:
            '               
            '
            ' Notes :
            '               
            ' -----------------------------------------------------------------------------

            rank = rank_in
            bib = bib_in
            name = name_in
            nat_code = natcode_in

            nation = MainConvertModule.nation_map(nat_code)
            If nation = "" Then nation = natcode_in

            time = time_in
            diff = diff_in

            shoot_1 = shoot_1_in
            shoot_2 = shoot_2_in
            shoot_3 = shoot_3_in
            shoot_4 = shoot_4_in
            shoot_total = shoot_total_in

            name = ConvertTools.RotateAndFixName(name)

            'Character map substitutions
            Dim subst As String() = character_map(nat_code)
            If Not subst Is Nothing Then
                Dim i As Integer
                For i = 0 To subst.GetLength(0) - 1
                    name = name.Replace(subst(i), subst(i + 1))
                    i += 1
                Next
            End If

        End Sub

        ' End of IBU_Results (Athlete) class
    End Class
    Protected athlete_list As ArrayList
    Protected Shared character_map As New Hashtable


    Public Sub New()
        ' -----------------------------------------------------------------------------
        ' Function:  NEW IBU_Results
        '
        ' Description:
        '               Object constructor creates object and loads mapping lists
        '               
        ' Parameters:
        '               
        '
        ' Returns:
        '               
        '
        ' Notes :
        '               
        ' -----------------------------------------------------------------------------

        athlete_list = New ArrayList

        'Load character map
        Dim charmapfile As String = My.Settings.CharacterMapFile ' .ProjectFolder(AppSettings("CharacterMapFile"))
        Dim map As New XmlDocument

        Try
            map.Load(charmapfile)

            Dim nodes As XmlNodeList
            Dim nd1, nd2 As XmlNode
            nodes = map.SelectNodes("/character-maps/nation")

            'Fill hash
            For Each nd1 In nodes
                If nd1.ChildNodes().Count > 0 Then

                    Dim c As Integer = 0
                    Dim charset((nd1.ChildNodes().Count * 2) - 1) As String
                    For Each nd2 In nd1.ChildNodes()
                        charset(c) = nd2.Attributes("value").Value
                        charset(c + 1) = nd2.Attributes("replace").Value
                        c += 2
                    Next

                    character_map(nd1.Attributes("code").Value) = charset
                End If
            Next

            WriteLog(New String(My.Settings.LogFolder), "IBU - Character converison map loaded (" & charmapfile & "): " & CStr(character_map.Count) & " conversion sets.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(New String(My.Settings.LogFolder), "Error loading charachter conversion map (" & charmapfile & ") for IBU", ex)
        End Try

    End Sub


    Protected Function ParseDataBI(ByVal filename As String) As Integer
        ' -----------------------------------------------------------------------------
        ' Function: ParseDataBI
        '
        ' Description:
        '               Takes a cross country HTML list and fills the athlete list
        '
        ' Parameters:
        '               filename:   The name of the HTML-file that contains the results
        '               
        '
        ' Returns:
        '               This function returns the number of athletes in the list so we can loop through it
        '               in another function... 
        '
        ' Notes :
        '               16.01.2008: Added getting debug settings from app.config-file so that we don't clutter 
        '                           the logfiler with lots of debug information. 
        '               
        ' -----------------------------------------------------------------------------

        Dim logFolder As String = My.Settings.LogFolder ' MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = My.Settings.Debug
        If Debug = True Then WriteLog(logFolder, "Da er vi i ParseDataBI")
        Dim input As StreamReader = New StreamReader(filename, Encoding.UTF8)
        Dim content As String = input.ReadToEnd()
        input.Close()

        athlete_list.Clear()

        'Remove tags
        Dim tags As Regex = New Regex("<[^>]+>", RegexOptions.Compiled) 'Drop all tags
        content = tags.Replace(content, "")

        'Split off header
        Dim topsplit As String = "Final Results"
        If content.IndexOf(topsplit) = -1 Then
            topsplit = "Provisional Results"
        End If
        content = content.Substring(content.IndexOf(topsplit) + topsplit.Length)

        'Find results
        Dim res As Regex = New Regex("^\s+=?(\d+|\w{3}) (\d+) ([\w -\.]+) (\w{3})( \d)?(  \d)?(  \d)?(  \d)?(\d+?)? ((\+?)([\d\.:]+))?", RegexOptions.IgnoreCase Or RegexOptions.Compiled Or RegexOptions.Multiline)

        Dim first As TimeSpan
        Dim m As Match = res.Match(content)
        While m.Success

            Dim rank As String = m.Groups(1).Value
            Dim bib As String = m.Groups(2).Value
            Dim name As String = m.Groups(3).Value
            Dim nation As String = m.Groups(4).Value
            Dim shoot_1 As String = m.Groups(5).Value.Trim()
            Dim shoot_2 As String = m.Groups(6).Value.Trim()
            Dim shoot_3 As String = m.Groups(7).Value.Trim()
            Dim shoot_4 As String = m.Groups(8).Value.Trim()
            Dim shoot_total As String = m.Groups(9).Value



            'Fetch the time data
            Dim time, diff As TimeSpan
            If m.Groups(11).Value = "" And m.Groups(12).Value <> "" Then
                first = GetTimeSpan(m.Groups(12).Value)
                time = first
            Else
                If m.Groups(11).Value <> "" Then
                    diff = GetTimeSpan(m.Groups(12).Value)
                    time = first.Add(diff)
                End If
            End If

            'Format times

            Dim strTime, strDiff As String
            strTime = ConvertTools.FormatTime(time)
            strDiff = ConvertTools.FormatTime(diff)

            Dim athlete As New athlete(rank, bib, name, nation, strTime, strDiff, shoot_1, shoot_2, shoot_3, shoot_4, shoot_total)
            athlete_list.Add(athlete)

            m = m.NextMatch()
        End While

        Return athlete_list.Count
    End Function


    Public Function BI_General(ByVal filename As String, ByVal format As MainConvertModule.FormatType, ByRef body As String) As String
        ' -----------------------------------------------------------------------------
        ' Function: BI_General
        '
        ' Description:
        '               Returns a formated resultlist, for individual starts
        '               
        ' Parameters:
        '               filename: The HTML-file that contains the results.
        '               format: In which jobfolder did the user save the file. Call the correct function based on this
        '               body: Return-string containing the resultlist
        '
        ' Returns:
        '           A list of results
        '               
        '
        ' Notes :
        '           Added logging functionality.
        '               
        ' -----------------------------------------------------------------------------
        Dim logFolder As String = My.Settings.LogFolder ' MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = My.Settings.Debug
        If Debug = True Then WriteLog(logFolder, "Da er vi i BI_General")

        'Fill the athlete list
        Dim c As Integer = ParseDataBI(filename)
        Dim ret As String = Nothing

        Dim i As Integer = 1
        Dim at As athlete
        For Each at In athlete_list

            'Add paragraps
            If i = 1 Then
                ret &= "<p class=""txt"">"
                ' ret &= "<p class=""Brdtekst"">"
            ElseIf i Mod 10 = 1 Then
                ret &= "</p>" & vbCrLf & "<p class=""txt-ind"">"
                ' ret &= "</p>" & vbCrLf & "<p class=""BrdtekstInnrykk"">"
            End If

            'Fill in results
            Select Case format
                Case MainConvertModule.FormatType.IndividualStart
                    ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.shoot_total & "), "
                Case Else
                    If i = 1 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.time & " (" & at.shoot_total & "), "
                    ElseIf i = 2 Then
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " min bak (" & at.shoot_total & "), "
                    Else
                        ret &= at.rank & ") " & at.name & ", " & at.nation & " " & at.diff & " (" & at.shoot_total & "), "
                    End If
            End Select

            'Update counter
            i += 1
        Next

        'Finish off and return
        ret &= "</p>"
        body = ret

        Return c
    End Function

End Class
