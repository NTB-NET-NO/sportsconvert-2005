Imports System.Text.RegularExpressions
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile

Module ConvertTools

    'Types of events
    Public Enum NameFormat
        NBSP
        UCASE
        BOTH
    End Enum

    
    Function RotateAndFixName(ByVal name As String, Optional ByVal format As NameFormat = NameFormat.BOTH, Optional ByVal lastNameFirst As Boolean = False) As String
        ' -----------------------------------------------------------------------------
        ' RotateAndFixName
        '
        ' Description:
        '               This function takes the name and rotates it so that first name comes first
        '               and the last name comes last. 
        '               It also sets capitals on first character in first name and last name
        '   
        ' Parameters:
        '               name:           The name that is to be rotated. This contains Lastname Firstname
        '               lastNameFirst:  If lastname shall be first or not.
        '
        ' Returns:
        '               This returns the rotated name
        '
        ' Notes :
        '               16.01.2008, Trond Hus�: Porting to VB 2005 and I had to change a few
        '                                       functions. 
        ' -----------------------------------------------------------------------------

        ' If we want to transcode this code to VB 2005, we need to change the way
        ' we get values from the app.config-file. This is very easy in VB 2005
        ' we just use my.settings.

        ' Dim logFolder As String = MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        ' Dim Debug As Boolean = MainConvertModule.ProjectFolder(AppSettings("Debug"))

        Dim logfolder As String = My.Settings.LogFolder
        Dim Debug As Boolean = My.Settings.Debug

        WriteLog(logFolder, "ConvertTools.RotateAndFixName")
        Dim nreg As New Regex("^([A-Z����� \-]+)\b ([\w \-]+)")
        Dim m As Match


        'Rotate first/last
        If Debug = True Then WriteLog(logFolder, "Rotate first/last")
        If format = NameFormat.BOTH Or format = NameFormat.UCASE Then
            m = nreg.Match(name)
            If m.Success() Then
                name = m.Groups(2).Value & " " & m.Groups(1).Value.ToLower
            End If
        End If

        ' Here we find &nbsp; (which is space in HTML, and removes it from the string)
        ' New for 2005: We have to change name.split.
        If format = NameFormat.BOTH Or format = NameFormat.NBSP Then
            If name.IndexOf("&nbsp;") > -1 Then
                name = name.Split("&").ToString(2) & " " & name.Split("&;").ToString(0)

                ' the line below is there because it is the one that worked in VB 2003. 
                ' name = name.Split("&;", 3)(2) & " " & name.Split("&;", 2)(0)
                name = name.Replace("&nbsp;", " ")
            End If
        End If

        name = name.ToLower

        'Uppercase -> Capital Case
        If Debug = True Then WriteLog(logFolder, "Uppercase -> Capital Case")
        nreg = New Regex("(\b|[ \-])(\w)")
        m = nreg.Match(name)
        While m.Success()
            name = name.Insert(m.Groups(2).Index, m.Groups(2).Value.ToUpper)
            name = name.Remove(m.Groups(2).Index + 1, 1)
            m = m.NextMatch()
        End While
        If Debug = True Then WriteLog(logFolder, "Ferdig med Uppercase -> Capital Case")
        'Dim delimStr As String = " "
        'Dim delimiter As Char() = delimStr.ToCharArray()
        'Dim split As String() = Nothing

        Dim s As String = Nothing

        'Split = name.Split(delimiter)
        'name = ""

        'For Each s In Split()
        's = s.Insert(0, s.Substring(0, 1).ToUpper())
        's = s.Remove(1, 1)
        'name = name & s & " "
        'Next

        'name = name.Trim(delimiter)
        If Debug = True Then WriteLog(logFolder, "Lastname first")
        If lastNameFirst = True Then
            ' Weijiang AN
            ' Det vi skal gj�re her er � flytte AN f�rst. 
            ' Med andre ord er det bare � flytte det bakerste navnet f�rst
            Dim split As String() = Nothing

            split = name.Split(" ")
            name = split(UBound(split)) & " " & split(LBound(split))

            'If lastNameFirst Then
            'name = name.Remove(name.LastIndexOf(s) - 1, s.Length + 1)
            'name = s & " " & name
            'End If
        End If
        WriteLog(logFolder, "Ferdig lastname first")
        WriteLog(logFolder, name)

        Return name
    End Function


    Function GetTimeSpan(ByVal time As String, Optional ByVal milli_mul As Integer = 100) As TimeSpan
        ' -----------------------------------------------------------------------------
        ' Function: GetTimeSpan
        '
        ' Description:  Convert the time-string into a timespan. This will be used when the transform the 
        '               time in formatTime. Time in Europe is like this: 1:22.10 (except in European cup Alpine where it is
        '               1:22,10.
        '               
        ' Parameters:
        '               time:       String containing the time
        '               milli_mul:  If this is set we shall multiply with the value, othervise it set to 100 and we
        '                           multiply with that value.
        ' Returns:
        '               This function returns the timestamped version of the timestring.
        '
        ' Notes :
        '               16.01.2008, Trond Hus�: For better VB 2005 implementation I have changed to my.settings
        '                                       in stead of using the VB 2003 way of getting values from the app.config-file
        '                                       I have also added Debug so that we don't mess up the logfile with a bunch
        '                                       of debug-values.
        ' -----------------------------------------------------------------------------


        Dim logFolder As String = My.Settings.LogFolder ' MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = My.Settings.Debug
        Dim ret As New TimeSpan(0)

        If Debug = True Then WriteLog(logFolder, "GetTimeSpan: This is input: " & time.ToString)
        ' We change if we have , in the time. it shall not be
        time = Replace(time.ToString, ",", ".")

        If Debug = True Then WriteLog(logFolder, "GetTimeSpan: This is input after replace: " & time.ToString)

        Dim diffRx As Regex = New Regex("((\d+).)?((\d+).)?(\d+).(\d+)")
        Dim diffM As Match = diffRx.Match(time)
        If diffM.Success Then
            Dim h As Integer = 0
            Dim m As Integer = 0
            Dim s As Integer = 0
            Dim t As Integer = 0

            If diffM.Groups(2).Value <> "" Then h = diffM.Groups(2).Value
            If diffM.Groups(4).Value <> "" Then
                m = diffM.Groups(4).Value
            Else
                m = h
                h = 0
            End If

            If diffM.Groups(5).Value <> "" Then s = diffM.Groups(5).Value
            If diffM.Groups(6).Value <> "" Then t = diffM.Groups(6).Value


            ret = New TimeSpan(0, h, m, s, t * milli_mul)
            ' ret = New TimeSpan(0, h, m, s, t)
        End If
        If Debug = True Then WriteLog(logFolder, "This is what we return from GetTimeSpan: " & ret.ToString)
        Return ret
    End Function


    Function FormatTime(ByVal span As TimeSpan, Optional ByVal milli_div As Integer = 100) As String
        ' -----------------------------------------------------------------------------
        ' Function: FormatTime
        '
        ' Description:
        '               This function formats the times in variable span according to NTB specs.
        '               
        ' Parameters:
        '               span:       the time transformed by getTimeSpan
        '               milli_div:  Value divided by this value so we get the correct time.
        '
        ' Returns:
        '               This function returns the formated time in a format that is specified by NTB
        '               
        '
        ' Notes :
        '               16.01.2008, Trond Hus�: Added debug so we don't clutter the logfile with a bunch of
        '                                       debug-messages.
        '               
        ' -----------------------------------------------------------------------------

        ' Added 100 to the integer to get the correct number out when parsing Cross Country mass start (Langrenn Felles)
        Dim logFolder As String = My.Settings.LogFolder ' MainConvertModule.ProjectFolder(AppSettings("LogFolder"))
        Dim Debug As Boolean = My.Settings.Debug
        Dim ret As String
        If Debug = True Then WriteLog(logFolder, "FormatTime: This is input: " & span.ToString)

        If span.Hours > 0 Then
            ret = String.Format("{0}.{1:D2}.{2:D2},{3}", span.Hours, span.Minutes, span.Seconds, span.Milliseconds / 100)
            'ElseIf time.Minutes > 0 Then
            '    strTime = String.Format("{0}.{1:D2},{2}", time.Minutes, time.Seconds, time.Milliseconds / 100)
        Else
            'strTime = String.Format("{0},{1}", time.Seconds, time.Milliseconds / 100)
            ret = String.Format("{0}.{1:D2},{2}", span.Minutes, span.Seconds, span.Milliseconds \ milli_div)
        End If
        If Debug = True Then WriteLog(logFolder, "This is what we return from FormatTime: " & ret.ToString)
        Return ret
    End Function


    Function FormatTimeAl(ByVal span As TimeSpan, Optional ByVal milli_div As Integer = 1) As String
        ' -----------------------------------------------------------------------------
        ' Function: FormatTimeAl
        '
        ' Description:
        '               This function formats the times in variable span according to NTB specs.
        '               Alpine is a tad bit different, that's why we have to have this other function
        '               
        ' Parameters:
        '               span:       the time transformed by getTimeSpan
        '               milli_div:  Value divided by this value so we get the correct time.
        '
        ' Returns:
        '               This function returns the formated time in a format that is specified by NTB
        '               
        '
        ' Notes :
        '               16.01.2008, Trond Hus�: Added debug so we don't clutter the logfile with a bunch of
        '                                       debug-messages.
        '                                       Also added logfile-"support" so we can get debug-messages if
        '                                       we set debug to true in the app.config-file. 
        '               
        ' -----------------------------------------------------------------------------

        Dim logFolder As String = My.Settings.LogFolder
        Dim Debug As Boolean = My.Settings.Debug

        If Debug = True Then WriteLog(logFolder, "FormatTimeAl: This is input: " & span.ToString)

        Dim ret As String


        If span.Hours > 0 Then
            ret = String.Format("{0}.{1:D2}.{2:D2},{3}", span.Hours, span.Minutes, span.Seconds, span.Milliseconds / 100)
            'ElseIf time.Minutes > 0 Then
            '    strTime = String.Format("{0}.{1:D2},{2}", time.Minutes, time.Seconds, time.Milliseconds / 100)
        Else
            'strTime = String.Format("{0},{1}", time.Seconds, time.Milliseconds / 100)
            ret = String.Format("{0}.{1:D2},{2:D2}", span.Minutes, span.Seconds, span.Milliseconds \ milli_div)
        End If

        If Debug = True Then WriteLog(logFolder, "Returning: " & ret.ToString)

        Return ret
    End Function

    'Formats times Skating according to NTB specs
    Function FormatTimeSkate(ByVal span As TimeSpan, Optional ByVal milli_div As Integer = 1) As String
        ' -----------------------------------------------------------------------------
        ' Function: FormatTimeSkate
        '
        ' Description:
        '               This function formats the times in variable span according to NTB specs.
        '               Skating is a tad bit different, that's why we have to have this other function
        '               
        ' Parameters:
        '               span:       the time transformed by getTimeSpan
        '               milli_div:  Value divided by this value so we get the correct time. Default = 1
        '
        ' Returns:
        '               This function returns the formated time in a format that is specified by NTB
        '               
        '
        ' Notes :
        '               16.01.2008, Trond Hus�: Added debug so we don't clutter the logfile with a bunch of
        '                                       debug-messages.
        '                                       Also added logfile-"support" so we can get debug-messages if
        '                                       we set debug to true in the app.config-file. 
        '               
        ' -----------------------------------------------------------------------------

        Dim ret As String
        Dim logFolder As String = My.Settings.LogFolder
        Dim Debug As Boolean = My.Settings.Debug

        If Debug = True Then WriteLog(logFolder, "FormatTimeSkate: This is input: " & span.ToString)

        If span.Minutes > 0 Then
            ret = String.Format("{0}.{1:D2},{2:D2}", span.Minutes, span.Seconds, span.Milliseconds \ milli_div)
        Else
            ret = String.Format("{0:D2},{1:D2}", span.Seconds, span.Milliseconds \ milli_div)
        End If

        If Debug = True Then WriteLog(logFolder, "Returning: " & ret.ToString)

        Return ret
    End Function

End Module
